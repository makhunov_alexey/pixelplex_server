const config = require('./../config/config');
const Schema = config.mongoose.Schema;

const articleSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    created_at: { 
        type: Date,
        default: Date.now
    },
    updated_at: { 
        type: Date,
        default: Date.now
    }
}, 
    { versionKey: false }
);

const Article = config.mongoose.model("article", articleSchema);
module.exports = Article;
