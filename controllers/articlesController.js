const Article = require('../models/articleModel')

module.exports.createArticle = async (req, res, next) => {
    try {
        if(req.body.title&&req.body.body){
            const article = new Article({
                title:req.body.title,
                body:req.body.body
            }) 
            await article.save();
            res.send(article)
        } else {
            res.status(422).send({errors:[{
                field:'empty field'
            }]});
        }    
    } catch (error) {
        res.status(422).send({errors:[{
            field:'error'
        }]});
    }
}

module.exports.updateArticle = async (req, res, next) => {
    try {
        if(req.body.title&&req.body.body){
            Article.findById(req.params.id,function(err,result){
                if(result){
                    const newTime = new Date();
                    result.update({title:req.body.title,body:req.body.body,updated_at:newTime}).exec();
                    res.send({_id:result._id,title:req.body.title,body:req.body.body,updated_at:newTime,created_at:result.created_at})
                }
            })
        } else {
            res.status(422).send({errors:[{
                field:'empty field'
            }]});
        }
    } catch (error) {
        res.status(422).send({errors:[{
            field:'error'
        }]});
    }
}

module.exports.getArticle = async (req, res, next) => {
    try {
        Article.findById(req.params.id,function(err,result){
            if(result){
                res.send(result)
            }else{
                res.status(422).send({errors:[{
                    field:'Article did not found'
                }]});
            }
        })
    } catch (error) {
        res.status(422).send({errors:[{
            field:'error'
        }]});
    }
}

module.exports.getArticles = async (req, res, next) => {
    try {
        const page = req.query.page?req.query.page:1;
        const limit = (req.query.limit&&req.query.limit<10)?req.query.limit:10;
        Article.find(function(err,result){
            if(result){
                res.send({count:result.length,page:page,limit:limit,articles:result.slice((page-1)*limit,limit*(page))})
            }else {
                res.status(422).send({errors:[{
                    field:'Do not found any article'
                }]});
            }
        })
    } catch (error) {
        res.status(422).send({errors:[{
            field:'error'
        }]});
    }
}