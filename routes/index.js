const express = require('express');
const router = express.Router();
const articlesController = require('../controllers/articlesController')

router.get('/', function (req, res, next) {
    console.log('good start');
    res.send({test:'test'})
});

router.post('/articles',articlesController.createArticle);
router.put('/articles/:id',articlesController.updateArticle);
router.get('/articles/:id',articlesController.getArticle);
router.get('/articles',articlesController.getArticles);  

module.exports = router;
